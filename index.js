
document.addEventListener('DOMContentLoaded', function () {
    // This will NOT execute when using async.
    // Change to defer only in the script tag to see this execute.
    console.log("hey?")
});


console.log('Global Context is Ready');

const elTitle = document.getElementById('title');
const elSubtitle = document.getElementById('subtitle');
const elTitleInput = document.getElementById('title-input');
const elSubtitleInput = document.getElementById('subtitle-input');
const elBtnReset = document.getElementById('btn-reset');

// When we type in Title Input
elTitleInput.addEventListener('keyup', function (event) {
    elTitle.innerText = event.target.value;
});

// When we type in Subtitle Input
elSubtitleInput.addEventListener('keyup', function(e) {
    elSubtitle.innerText = e.target.value;
});


// When we click the Reset button
elBtnReset.addEventListener('click', function() {
    // Clearing inner text of HTML elements
    elTitle.innerText = '';
    elSubtitle.innerHTML = '';
    // Clearing values of inputs.
    elTitleInput.value = '';
    elSubtitleInput.value = '';
})

# DotNet - JavaScript Class Demo 001

The first class demo investigating how to access DOM elements and manipulate values. Also takes a look at attaching event listeners to input elements and how to handle the execution of an event listener.
